# Maschinenlesbare Lizeninformationen unter Bilder einfügen

Die CC Lizenzen basieren auf einem [Dreischichten-Konzept](https://creativecommons.org/licenses/):

- Lizenzvertrag: Traditionelle Lizenz, die in der Ausdrucksweise der Juristen gehalten ist.
- Commons Deed: Die menschenlesbare Lizenz, die auf praktische Art und Weise von Lizenzgeber:innen und Lizenznehmer:innen verwendetet werden kann
- CC Rights Expression Language ([CC REL](https://wiki.creativecommons.org/wiki/CC_REL): Maschinenlesbare Lizenz für Suchmaschinen und Software).

## Easy to implement, but not machine readable

Das Markdown sollte den Code enthalten, um das Bild anzuzeigen und in einer zweiten Zeile
sollte die menschenlesbare Lizenzinformation hinterlegt sein. 

```html
![Brown Cat With Green Eyes]((https://i.imgur.com/WiYXnnp.jpg) "Brown Cat With Green Eyes")

*"[Brown Cat With Green Eyes](https://i.imgur.com/WiYXnnp.jpg)" by [Kelvin Valerio](https://www.pexels.com/@kelvin809) licensed under [CC 0](https://creativecommons.org/publicdomain/zero/1.0/) from [Pexels](https://www.pexels.com/photo/adorable-animal-blur-cat-617278/)*
```

![Brown Cat With Green Eyes](https://i.imgur.com/ukhHgII.jpg)

*"[Brown Cat With Green Eyes](https://i.imgur.com/ukhHgII.jpg)" by [Kelvin Valerio](https://www.pexels.com/@kelvin809) licensed under [CC 0](https://creativecommons.org/publicdomain/zero/1.0/) from [Pexels](https://www.pexels.com/photo/adorable-animal-blur-cat-617278/)*

## Nicht so leicht einzufügen, aber maschinenlesbar  (und CC REL-konform)

Die maschinenlesbare Lizenz kann leider momentan noch nicht in simplem Markdown
hinterlegt werden, sondern muss in HTML geschrieben werden.

[Dieses Projekt](https://gitlab.com/TIBHannover/oer/course-metadata-test) beschäftigt
sich jedoch damit, wie diese Informationen automatisch implementiert werden können.
Momentan können die Lizenzinfomrationen, wenn das Bild von Wikimedia stammt,
bereits automatisch hinterlegt werden.

~~~html
<div about="https://i.imgur.com/WiYXnnp.jpg" class="figure"> 
 <img src="https://i.imgur.com/WiYXnnp.jpg" alt="Brown Cat With Green Eyes" /> 
 <span property="dc:title">Brown Cat With Green Eyes</span> 
 by <a rel="cc:attributionURL dc:creator" href="https://www.pexels.com/@kelvin809" property="cc:attributionName">Kelvin Valerio</a> under <a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/">CC 0</a> from <a rel=\"dc: source\" href="https://www.pexels.com/photo/adorable-animal-blur-cat-617278/">Pexels</a>
</div>
~~~

<div about="https://i.imgur.com/WiYXnnp.jpg" class="figure"> 
 <img src="https://i.imgur.com/WiYXnnp.jpg" alt="Brown Cat With Green Eyes" /> 
 <span property="dc:title">Brown Cat With Green Eyes</span> 
 by <a rel="cc:attributionURL dc:creator" href="https://www.pexels.com/@kelvin809" property="cc:attributionName">Kelvin Valerio</a> under <a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/">CC 0</a> from <a rel=\"dc: source\" href="https://www.pexels.com/photo/adorable-animal-blur-cat-617278/">Pexels</a>
</div>


## More images for testing

<div about="https://upload.wikimedia.org/wikipedia/commons/2/20/Peafowl_Courtship.jpg" class="figure"> 
 <img src="https://upload.wikimedia.org/wikipedia/commons/2/20/Peafowl_Courtship.jpg" alt="Peafowl Courtship" /> 
 <span property="dc:title">Peafowl Courtship</span> 
 by <a rel="cc:attributionURL dc:creator" href="https://commons.wikimedia.org/wiki/User:Clément Bardot" property="cc:attributionName">Clément Bardot</a> under <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a> 
</div>

<div about="https://upload.wikimedia.org/wikipedia/commons/d/d1/01-Rechtwinkliges_Dreieck-Pythagoras.svg" class="figure"> 
 <img src="https://upload.wikimedia.org/wikipedia/commons/d/d1/01-Rechtwinkliges_Dreieck-Pythagoras.svg" alt="01-Rechtwinkliges Dreieck-Pythagoras" /> 
 <span property="dc:title">01-Rechtwinkliges Dreieck-Pythagoras</span> 
 by <a rel="cc:attributionURL dc:creator" href="https://commons.wikimedia.org/wiki/User:Petrus3743" property="cc:attributionName">Petrus3743</a> under <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a> 

## Hier gibt es das Dokument in [HTML](https://sroertgen.gitlab.io/maschinenlesbare-cc-lizenz/index.html), [PDF](https://sroertgen.gitlab.io/maschinenlesbare-cc-lizenz/course.pdf) oder als [EPUB](https://sroertgen.gitlab.io/maschinenlesbare-cc-lizenz/course.epub)
